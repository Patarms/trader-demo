# Github Simple Client - with Offline capabilities #

Demo GitHub info-client, featuring RxJava, Retrofit, OkHttp, Picasso, SQLite

# Setup #
```
mkdir trader-peter-demo

git clone https://Patarms@bitbucket.org/Patarms/trader-demo.git ./trader-peter-demo

# Linux Specific
<android-studio-path-here>/bin/studio.sh ./trader-peter-demo
```