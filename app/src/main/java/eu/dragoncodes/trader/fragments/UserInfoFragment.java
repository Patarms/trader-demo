package eu.dragoncodes.trader.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import eu.dragoncodes.trader.R;
import eu.dragoncodes.trader.models.GitHubUser;
import eu.dragoncodes.trader.network.GitHubClient;
import eu.dragoncodes.trader.utils.ActivityUtils;
import eu.dragoncodes.trader.utils.Constants;
import eu.dragoncodes.trader.views.UrlImageView;
import io.reactivex.functions.Consumer;

public class UserInfoFragment extends UsernameDependentFragment {

    private final Consumer<GitHubUser> mOnNextAction = new Consumer<GitHubUser>() {
        @Override
        public void accept(@io.reactivex.annotations.NonNull GitHubUser gitHubUser) throws Exception {
            mGitHubUser = gitHubUser;

            inflateViews();

            showHideProgressBar(false);
        }
    };

    private final Consumer<Throwable> mOnError = new Consumer<Throwable>() {
        @Override
        public void accept(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
            Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private GitHubUser mGitHubUser;

    // Views
    private UrlImageView mAvatarView;

    private TextView mNameView;
    private TextView mBioView;
    private TextView mLocationView;
    private TextView mFollowersView;
    private TextView mFollowingView;
    private View mContentView;

    private ProgressBar mProgressBar;

    public UserInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_user_info, container, false);

        mAvatarView = (UrlImageView) layout.findViewById(R.id.iv_avatar);

        mBioView = (TextView) layout.findViewById(R.id.tv_bio);
        mNameView = (TextView) layout.findViewById(R.id.tv_name);
        mLocationView = (TextView) layout.findViewById(R.id.tv_location);
        mFollowersView = (TextView) layout.findViewById(R.id.tv_followers);
        mFollowingView = (TextView) layout.findViewById(R.id.tv_following);

        mContentView = layout.findViewById(R.id.rl_content);
        mProgressBar = (ProgressBar) layout.findViewById(R.id.pb_loading);

        mFollowersView.setOnClickListener(getFollowersListener(Constants.USER_LIST_TYPE_FOLLOWERS));
        mFollowingView.setOnClickListener(getFollowersListener(Constants.USER_LIST_TYPE_FOLLOWING));

        return layout;
    }

    private View.OnClickListener getFollowersListener(final String userListActivityType) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startUserListActivity(getActivity(), mUsername, userListActivityType);
            }
        };
    }

    private void inflateViews() {
        mAvatarView.setUrl(mGitHubUser.getAvatarUrl());


        String followers = String.format(getString(R.string.label_followers), mGitHubUser.getFollowersCount() + "");

        String following = String.format(getString(R.string.label_following), mGitHubUser.getFollowingCount() + "");

        mNameView.setText(mGitHubUser.getName());

        String bio = mGitHubUser.getBio();
        String location = mGitHubUser.getLocation();

        if (TextUtils.isEmpty(bio)) {
            mBioView.setVisibility(View.GONE);
        } else {
            mBioView.setText(mGitHubUser.getBio());
        }

        if (TextUtils.isEmpty(location)) {
            mLocationView.setVisibility(View.GONE);
        } else {
            mLocationView.setText(mGitHubUser.getLocation());
        }

        mFollowersView.setText(followers);
        mFollowingView.setText(following);
    }

    @Override
    protected void onUsernameSet() {

        GitHubClient client = new GitHubClient(getContext());

        client.getUserDetails(mUsername).subscribe(mOnNextAction, mOnError);
    }

    @Override
    protected void showHideProgressBar(boolean showHide) {
        mContentView.setVisibility(showHide ? View.GONE : View.VISIBLE);
        mProgressBar.setVisibility(showHide ? View.VISIBLE : View.GONE);
    }
}
