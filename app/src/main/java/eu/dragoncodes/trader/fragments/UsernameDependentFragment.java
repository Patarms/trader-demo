package eu.dragoncodes.trader.fragments;


import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

public abstract class UsernameDependentFragment extends Fragment {

    protected String mUsername;

    public void setUsername(@NonNull String username) {
        mUsername = username;

        onUsernameSet();
    }

    protected abstract void onUsernameSet();

    protected abstract void showHideProgressBar(boolean showHide);
}
