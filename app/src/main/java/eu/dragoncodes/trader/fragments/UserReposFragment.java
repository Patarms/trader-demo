package eu.dragoncodes.trader.fragments;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import eu.dragoncodes.trader.R;
import eu.dragoncodes.trader.models.GitHubRepository;
import eu.dragoncodes.trader.network.GitHubClient;
import eu.dragoncodes.trader.utils.Generic;
import eu.dragoncodes.trader.views.adapters.BaseAdapter;
import eu.dragoncodes.trader.views.adapters.ReposAdapter;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class UserReposFragment extends UsernameDependentFragment implements BaseAdapter.OnItemClickedListener<GitHubRepository> {

    // Views
    private RecyclerView mReposListView;
    private ReposAdapter mReposAdapter;

    private ProgressBar mProgressBar;


    private final Consumer<List<GitHubRepository>> onNextAction = new Consumer<List<GitHubRepository>>() {
        @Override
        public void accept(@NonNull List<GitHubRepository> gitHubRepositories) throws Exception {
            populateAdapter(gitHubRepositories);
        }
    };

    private final Consumer<Throwable> onErrorAction = new Consumer<Throwable>() {
        @Override
        public void accept(@NonNull Throwable throwable) throws Exception {
            Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();

            showHideProgressBar(false);
        }
    };

    public UserReposFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_user_repos, container, false);

        mProgressBar = (ProgressBar) layout.findViewById(R.id.pb_loading);

        mReposAdapter = new ReposAdapter();

        mReposAdapter.setOnItemClickListener(this);

        mReposListView = (RecyclerView) layout.findViewById(R.id.rv_repos);
        mReposListView.setAdapter(mReposAdapter);
        mReposListView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        return layout;
    }

    private void populateAdapter(List<GitHubRepository> repos) {
        mReposAdapter.setItems(repos);

        showHideProgressBar(false);
    }

    @Override
    protected void onUsernameSet() {
        GitHubClient client = new GitHubClient(getContext());

        client.getUserRepos(mUsername).subscribe(onNextAction, onErrorAction);
    }

    @Override
    protected void showHideProgressBar(boolean showHide) {
        mProgressBar.setVisibility(showHide ? View.VISIBLE : View.GONE);
        mReposListView.setVisibility(showHide ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onItemClicked(GitHubRepository item) {
        Generic.openUrl(getContext(), item.getUrl());
    }
}
