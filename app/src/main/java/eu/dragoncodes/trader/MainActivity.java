package eu.dragoncodes.trader;

import android.os.Bundle;

import eu.dragoncodes.trader.models.OnUserAuthenticatedListener;
import eu.dragoncodes.trader.utils.ActivityUtils;

public class MainActivity extends AuthenticatedActivity implements OnUserAuthenticatedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void userAuthenticated() {
        ActivityUtils.startUserDetailsActivity(this, mAccountManager.getUsername());
        finish();
    }
}
