package eu.dragoncodes.trader.utils;

import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import eu.dragoncodes.trader.managers.caching.GitHubFollowersCaching;
import eu.dragoncodes.trader.managers.caching.GitHubRepositoryCaching;
import eu.dragoncodes.trader.managers.caching.GitHubUserCaching;
import eu.dragoncodes.trader.models.GitHubRepository;
import eu.dragoncodes.trader.models.GitHubUser;

public class Sql {

    private static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS  %s ( _id INTEGER PRIMARY KEY, %s );";
    private static final String DROP_SQL = "DROP TABLE IF EXISTS %s;";

    private static String generateUserCreateSql() {

        String result = "";

        for (Field field : GitHubUser.class.getDeclaredFields()) {
            Class type = field.getType();

            Annotation[] annotations = field.getDeclaredAnnotations();

            if (annotations.length == 0 || !(annotations[0] instanceof SerializedName)) {
                continue;
            }

            // Get the GSON defined name and assign it a type depending on the class definition type
            result += ((SerializedName) (annotations[0])).value() + " " + (type.getName().contains("String") ? "TEXT" : "INTEGER") + ",";
        }

        // Remove trailing commas
        result = " id INTEGER, " + result.substring(0, result.lastIndexOf(','));

        return String.format(CREATE_SQL, GitHubUserCaching.TABLE_NAME, result);
    }

    private static String generateRepositoryCreateSql() {

        String result = "";

        for (Field field : GitHubRepository.class.getDeclaredFields()) {
            Class type = field.getType();
            Annotation[] annotations = field.getDeclaredAnnotations();

            if (annotations.length == 0 || !(annotations[0] instanceof SerializedName)) {
                continue;
            }

            String annotationValue = ((SerializedName) (annotations[0])).value();

            if (annotationValue.equals("owner")) {
                result += "owner_login TEXT,";
            } else {

                // Get the GSON defined name and assign it a type depending on the class definition type
                result += annotationValue + " " + (type.getName().contains("String") ? "TEXT" : "INTEGER") + ",";
            }
        }

        result = " id INTEGER, " + result + " CONSTRAINT name_unique UNIQUE (name) ON CONFLICT REPLACE";

        return String.format(CREATE_SQL, GitHubRepositoryCaching.TABLE_NAME, result);
    }

    private static String generateFollowersCreateSql() {
        String template = " follower_username TEXT, following_username TEXT, UNIQUE(follower_username, following_username) ON CONFLICT REPLACE";
        return String.format(CREATE_SQL, GitHubFollowersCaching.TABLE_NAME, template);
    }

    public static String[] getCreateSql() {
        String sql = generateRepositoryCreateSql() + generateUserCreateSql() + generateFollowersCreateSql();

        return sql.split(";");
    }

    public static String[] getDeletionSql() {
        String sql = String.format(DROP_SQL, GitHubUserCaching.TABLE_NAME) + String.format(DROP_SQL, GitHubFollowersCaching.TABLE_NAME);

        sql += String.format(DROP_SQL, GitHubRepositoryCaching.TABLE_NAME);

        return sql.split(";");
    }

}
