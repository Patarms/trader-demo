package eu.dragoncodes.trader.utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import eu.dragoncodes.trader.UserDetailsActivity;
import eu.dragoncodes.trader.UserListActivity;

public class ActivityUtils {

    private static Intent prepareUsernameIntent(Context context, String username, Class className) {

        Intent intent = new Intent(context, className);
        intent.putExtra(Constants.USERNAME_KEY, username);

        return intent;
    }

    private static void startActivityWithUsername(Context context, String username, Class className) {

        context.startActivity(prepareUsernameIntent(context, username, className));
    }

    public static void startUserDetailsActivity(@NonNull Context context, @NonNull String username) {
        startActivityWithUsername(context, username, UserDetailsActivity.class);
    }

    public static void startUserListActivity(@NonNull Context context, @NonNull String username, @NonNull String userListType) {
        Intent intent = prepareUsernameIntent(context, username, UserListActivity.class);

        intent.putExtra(Constants.USER_LIST_TYPE_KEY, userListType);

        context.startActivity(intent);
    }
}
