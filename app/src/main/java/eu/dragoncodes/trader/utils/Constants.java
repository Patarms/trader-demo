package eu.dragoncodes.trader.utils;

public class Constants {

    public static final String USER_ID_KEY = "userId";
    public static final String USERNAME_KEY = "username";

    public static final String GITHUB_CLIENT_ID_KEY = "client_id";
    public static final String GITHUB_CLIENT_SECRET_KEY = "client_secret";

    public static final String USER_LIST_TYPE_KEY = "user_list_type";
    public static final String USER_LIST_TYPE_FOLLOWERS = "user_followers";
    public static final String USER_LIST_TYPE_FOLLOWING = "user_following";
}
