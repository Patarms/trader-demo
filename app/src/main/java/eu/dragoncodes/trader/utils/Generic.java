package eu.dragoncodes.trader.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

public class Generic {
    public static void openUrl(@NonNull Context context, @NonNull String url) {

        if (!TextUtils.isEmpty(url)) {

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            context.startActivity(intent);
        }
    }
}
