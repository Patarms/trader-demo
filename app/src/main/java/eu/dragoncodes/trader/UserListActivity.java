package eu.dragoncodes.trader;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import eu.dragoncodes.trader.models.GitHubUser;
import eu.dragoncodes.trader.network.GitHubClient;
import eu.dragoncodes.trader.utils.ActivityUtils;
import eu.dragoncodes.trader.utils.Constants;
import eu.dragoncodes.trader.views.adapters.BaseAdapter;
import eu.dragoncodes.trader.views.adapters.UserListAdapter;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class UserListActivity extends UsernameAuthActivity implements BaseAdapter.OnItemClickedListener<GitHubUser> {

    private final Consumer<List<GitHubUser>> mOnNextAction = new Consumer<List<GitHubUser>>() {
        @Override
        public void accept(@NonNull List<GitHubUser> gitHubUsers) throws Exception {
            mUserListAdapter.setItems(gitHubUsers);

            showHideProgress(false);

        }
    };

    private final Consumer<Throwable> mOnErrorAction = new Consumer<Throwable>() {
        @Override
        public void accept(@NonNull Throwable throwable) throws Exception {
            showHideProgress(false);
            Toast.makeText(UserListActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private String mUserListType;

    private ProgressBar mProgressBar;
    private RecyclerView mUserListView;
    private UserListAdapter mUserListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        mUserListView = (RecyclerView) findViewById(R.id.rv_user_list);

        mUserListAdapter = new UserListAdapter();
        mUserListAdapter.setOnItemClickListener(this);

        mProgressBar = (ProgressBar) findViewById(R.id.pb_loading);

        mUserListView.setAdapter(mUserListAdapter);
        mUserListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        Intent intent = getIntent();

        if (intent.hasExtra(Constants.USER_LIST_TYPE_KEY)) {
            mUserListType = intent.getStringExtra(Constants.USER_LIST_TYPE_KEY);
        }

        fetchUserList();
    }

    private void fetchUserList() {
        GitHubClient client = new GitHubClient(this);

        Observable<List<GitHubUser>> observable;

        int stringResId = R.string.followers_template;

        if (mUserListType.equals(Constants.USER_LIST_TYPE_FOLLOWERS)) {
            observable = client.getFollowers(mUsername);
        } else {
            observable = client.getFollowing(mUsername);

            stringResId = R.string.following_template;
        }

        setTitle(String.format(getString(stringResId), mUsername));

        observable.subscribe(mOnNextAction, mOnErrorAction);
    }

    @Override
    public void onItemClicked(GitHubUser item) {
        ActivityUtils.startUserDetailsActivity(this, item.getUsername());
    }

    private void showHideProgress(boolean showHide) {

        mProgressBar.setVisibility(showHide ? View.VISIBLE : View.GONE);
        mUserListView.setVisibility(showHide ? View.GONE : View.VISIBLE);
    }
}
