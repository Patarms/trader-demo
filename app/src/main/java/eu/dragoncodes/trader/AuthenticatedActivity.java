package eu.dragoncodes.trader;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import eu.dragoncodes.trader.managers.AccountManager;
import eu.dragoncodes.trader.models.OnUserAuthenticatedListener;

public abstract class AuthenticatedActivity extends AppCompatActivity {

    protected AccountManager mAccountManager;

    @Override
    protected void onStart() {
        super.onStart();

        mAccountManager = new AccountManager(this);

        checkUserStatus();
    }

    protected void checkUserStatus() {

        if (mAccountManager.getUserId() == AccountManager.NO_USER) {
            requestLogin();
        } else if (this instanceof OnUserAuthenticatedListener) {
            ((OnUserAuthenticatedListener) this).userAuthenticated();
        }
    }

    protected String getUsername() {
        return mAccountManager.getUsername();
    }

    protected void logout() {

        mAccountManager.setUserId(AccountManager.NO_USER);

        requestLogin();
    }

    protected void requestLogin() {
        startActivity(new Intent(this, LoginActivity.class));

        finish();
    }
}
