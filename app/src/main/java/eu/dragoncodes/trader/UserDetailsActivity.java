package eu.dragoncodes.trader;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import eu.dragoncodes.trader.fragments.UserInfoFragment;
import eu.dragoncodes.trader.fragments.UserReposFragment;

public class UserDetailsActivity extends UsernameAuthActivity {

    private UserInfoFragment mUserInfoFragment;
    private UserReposFragment mUserReposFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        mUserReposFragment = (UserReposFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_user_repos);
        mUserInfoFragment = (UserInfoFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_user_info);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        setTitle(mUsername);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Show logout button if the user is the current logged in
        if (getUsername().equals(mUsername)) {
            getMenuInflater().inflate(R.menu.menu_main, menu);

            menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    logout();
                    return false;
                }
            });

            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mUserReposFragment.setUsername(mUsername);
        mUserInfoFragment.setUsername(mUsername);
    }
}
