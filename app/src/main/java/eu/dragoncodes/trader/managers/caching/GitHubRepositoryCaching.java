package eu.dragoncodes.trader.managers.caching;

import android.content.Context;
import android.database.Cursor;

import java.util.List;

import eu.dragoncodes.trader.models.GitHubRepository;

public class GitHubRepositoryCaching extends SqlCachingAdapter<GitHubRepository> {

    public static final String TABLE_NAME = "git_repo";

    public GitHubRepositoryCaching(Context context) {
        super(context);
    }

    @Override
    protected GitHubRepository fromCursor(Object id, String tag, Cursor cursor) {
        return GitHubRepository.fromCursor(cursor);
    }

    @Override
    protected String getSelectQuery(Object id, String tag) {
        return "SELECT * from " + TABLE_NAME + " where owner_login='" + id + "'";
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
