package eu.dragoncodes.trader.managers.caching;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import eu.dragoncodes.trader.models.ContentValueParcelable;
import eu.dragoncodes.trader.utils.Sql;

/**
 * The current offline mode for this app is handled via this adapter
 * To avoid thread collision I opted for the not so great solution of a Singleton @link({@link SQLiteOpenHelper}).
 * A far better solution would be using @link({@link android.content.ContentProvider}), but time constraints
 * made the choice.
 * @param <T>
 */
abstract class SqlCachingAdapter<T extends ContentValueParcelable> extends CachingAdapter<T> {

    private static final String DB_NAME = "TraderGithub.db";
    private static final int DB_VERSION = 1;

    private final Context mContext;

    public SqlCachingAdapter(Context context) {
        mContext = context;
    }

    @Override
    protected T getObjectById(Object id, String tag) {
        SQLiteDatabase db = SQLiteHelper.getInstance(mContext).getReadableDatabase();

        Cursor cursor = null;
        T item = null;
        try {
            cursor = db.rawQuery(getSelectQuery(id, tag), null);

            if (cursor.moveToFirst()) {
                item = fromCursor(id, tag, cursor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return item;
    }

    @Override
    protected synchronized void writeObject(T object, String tag) {

        try {

            ContentValues values = object.toContentValues();

            SQLiteDatabase db = SQLiteHelper.getInstance(mContext).getWritableDatabase();
            db.insertWithOnConflict(getTableName(), null, values, SQLiteDatabase.CONFLICT_IGNORE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected List<T> getObjectsById(Object id, String tag) {
        SQLiteDatabase db = SQLiteHelper.getInstance(mContext).getReadableDatabase();

        List<T> result = new ArrayList<>();

        Cursor cursor = null;

        try {
            cursor = db.rawQuery(getSelectQuery(id, tag), null);

            if (cursor.moveToFirst()) {
                do {

                    T item = fromCursor(id, tag, cursor);
                    result.add(item);
                }
                while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return result;
    }

    protected abstract T fromCursor(Object id, String tag, Cursor cursor);

    protected abstract String getSelectQuery(Object id, String tag);

    protected abstract String getTableName();

    public static class SQLiteHelper extends SQLiteOpenHelper {

        private static SQLiteHelper sInstance;

        public static synchronized SQLiteHelper getInstance(Context context) {

            if (sInstance == null) {
                sInstance = new SQLiteHelper(context.getApplicationContext());
            }
            return sInstance;
        }

        private SQLiteHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            String[] createQueries = Sql.getCreateSql();

            for (String createQuery : createQueries) {
                db.execSQL(createQuery);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            String[] deletionSqls = Sql.getDeletionSql();

            for (String deletionSql : deletionSqls) {
                db.execSQL(deletionSql);
            }

            onCreate(db);
        }
    }
}
