package eu.dragoncodes.trader.managers.caching;

import android.content.Context;
import android.database.Cursor;
import java.util.List;

import eu.dragoncodes.trader.models.GitHubUser;

public class GitHubUserCaching extends SqlCachingAdapter<GitHubUser> {

    public static final String TABLE_NAME = "git_user";

    public GitHubUserCaching(Context context) {
        super(context);
    }

    @Override
    protected GitHubUser fromCursor(Object id, String tag, Cursor cursor) {
        return GitHubUser.fromCursor(cursor);
    }

    @Override
    protected String getSelectQuery(Object id, String tag) {
        return "SELECT * from " + getTableName() + " WHERE login='" + id + "'";
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
