package eu.dragoncodes.trader.managers;

import android.content.Context;
import android.support.annotation.NonNull;

import static eu.dragoncodes.trader.utils.Constants.USERNAME_KEY;
import static eu.dragoncodes.trader.utils.Constants.USER_ID_KEY;

public class AccountManager extends SPUser {

    public static final long NO_USER = -1;

    private static final String DIRECTORY_NAME = "account_details";

    public AccountManager(Context context) {
        super(context, DIRECTORY_NAME);
    }

    public long getUserId() {
        return getSharedPreferences().getLong(USER_ID_KEY, -1);
    }

    public String getUsername() {
        return getSharedPreferences().getString(USERNAME_KEY, null);
    }

    public void setUserId(long userId) {
        getSharedPreferences().edit().putLong(USER_ID_KEY, userId).apply();
    }

    public void setUsername(@NonNull String username) {
        getSharedPreferences().edit().putString(USERNAME_KEY, username).apply();
    }
}
