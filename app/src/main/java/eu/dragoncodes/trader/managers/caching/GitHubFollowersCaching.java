package eu.dragoncodes.trader.managers.caching;

import android.content.Context;
import android.database.Cursor;

import java.util.Collections;
import java.util.List;

import eu.dragoncodes.trader.models.GitHubFollower;

public class GitHubFollowersCaching extends SqlCachingAdapter<GitHubFollower> {

    public static final String FOLLOWERS_TAG = "followers_get";
    public static final String FOLLOWING_TAG = "following_get";

    public static final String TABLE_NAME = "git_follower";

    public GitHubFollowersCaching(Context context) {
        super(context);
    }

    @Override
    protected GitHubFollower fromCursor(Object id, String tag, Cursor cursor) {

        GitHubFollower follower = new GitHubFollower();

        try {
            String followerUsername = cursor.getString(cursor.getColumnIndexOrThrow("follower_username"));
            String followingUsername = cursor.getString(cursor.getColumnIndexOrThrow("following_username"));

            follower.setFollowerUsername(followerUsername);
            follower.setFollowingUsername(followingUsername);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return follower;
    }

    @Override
    protected List<GitHubFollower> getObjectsById(Object id, String tag) {
        List<GitHubFollower> res = super.getObjectsById(id, tag);

        Collections.reverse(res);
        return res;
    }

    @Override
    protected String getSelectQuery(Object id, String tag) {

        String whereClause = tag.equals(FOLLOWERS_TAG) ? "follower_username=" : "following_username=";

        return "SELECT * from " + getTableName() + " WHERE " + whereClause + "'" + id + "'";
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }
}
