package eu.dragoncodes.trader.managers.caching;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import eu.dragoncodes.trader.models.GitHubFollower;
import eu.dragoncodes.trader.models.GitHubRepository;
import eu.dragoncodes.trader.models.GitHubUser;

public class GitHubCaching {

    private final GitHubUserCaching mGitHubUserCaching;
    private final GitHubFollowersCaching mGitHubFollowerCaching;
    private final GitHubRepositoryCaching mGitHubRepositoryCaching;

    private static GitHubCaching sInstance;

    public static synchronized GitHubCaching getCachingInstance(Context context) {
        if (sInstance == null)
            sInstance = new GitHubCaching(context.getApplicationContext());

        return sInstance;
    }

    private GitHubCaching(Context context) {

        mGitHubUserCaching = new GitHubUserCaching(context);
        mGitHubFollowerCaching = new GitHubFollowersCaching(context);
        mGitHubRepositoryCaching = new GitHubRepositoryCaching(context);
    }

    public void saveUser(GitHubUser user) {
        mGitHubUserCaching.writeObject(user, null);
    }

    public GitHubUser getUser(Object id) {
        return mGitHubUserCaching.getObjectById(id, null);
    }

    public void saveFollower(GitHubFollower follower) {
        mGitHubFollowerCaching.writeObject(follower, null);
    }

    public void saveFollowers(@NonNull String username, List<GitHubUser> users) {

        for (int i = 0; i < users.size(); i++) {
            saveFollower(new GitHubFollower(username, users.get(i).getUsername()));
        }
    }

    public List<GitHubFollower> getFollowers(String username) {
        return mGitHubFollowerCaching.getObjectsById(username, GitHubFollowersCaching.FOLLOWING_TAG);
    }

    public List<GitHubFollower> getFollowing(String username) {
        return mGitHubFollowerCaching.getObjectsById(username, GitHubFollowersCaching.FOLLOWERS_TAG);
    }

    public void saveRepository(GitHubRepository repository) {
        mGitHubRepositoryCaching.writeObject(repository, null);
    }

    public List<GitHubRepository> getRepositories(String ownerLogin) {
        return mGitHubRepositoryCaching.getObjectsById(ownerLogin, null);
    }
}