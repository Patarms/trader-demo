package eu.dragoncodes.trader.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public abstract class SPUser {

    protected String mDirectoryName;
    protected Context mContext;

    protected SPUser(@NonNull Context context, @NonNull String directoryName) {
        mDirectoryName = directoryName;
        mContext = context;
    }

    protected SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(mDirectoryName, Context.MODE_PRIVATE);
    }
}
