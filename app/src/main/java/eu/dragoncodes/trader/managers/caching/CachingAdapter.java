package eu.dragoncodes.trader.managers.caching;

import java.util.List;

import eu.dragoncodes.trader.models.UniqueItem;

/**
 * A generic caching adapter.
 * Theoretically, basic caching could be implemented with this via SQLite or plain JSON file storage
 * (or shared preferences for light amounts of data)
 * @param <T>
 */
abstract class CachingAdapter<T extends UniqueItem> {

    protected abstract T getObjectById(Object id, String tag);

    protected abstract List<T> getObjectsById(Object id, String tag);

    protected abstract void writeObject(T object, String tag);
}
