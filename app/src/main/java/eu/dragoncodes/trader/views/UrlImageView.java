package eu.dragoncodes.trader.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import eu.dragoncodes.trader.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class UrlImageView extends AppCompatImageView {
    public UrlImageView(Context context) {
        super(context);
    }

    public UrlImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UrlImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setUrl(@NonNull String url) {
        Picasso.with(getContext())
                .load(url)
                .placeholder(R.drawable.ic_restore_black_24dp)
                .error(R.drawable.ic_report_problem_black_24dp)
                .into(this);
    }
}
