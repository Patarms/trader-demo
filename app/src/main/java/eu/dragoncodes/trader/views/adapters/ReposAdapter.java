package eu.dragoncodes.trader.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;

import eu.dragoncodes.trader.R;
import eu.dragoncodes.trader.models.GitHubRepository;

public class ReposAdapter extends BaseAdapter<ReposAdapter.RepoViewHolder, GitHubRepository> {

    private RecyclerView mRecyclerInstance;

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_item, parent, false);
        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepoViewHolder holder, int position) {
        holder.repository = mItems.get(position);
        holder.bindView(position);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mRecyclerInstance = recyclerView;
    }

    @Override
    protected void postProcessItems() {
        Collections.sort(mItems, new Comparator<GitHubRepository>() {
            @Override
            public int compare(GitHubRepository r1, GitHubRepository r2) {
                if (r1.getForkCount() == r2.getForkCount()) {
                    boolean isGreater = r1.getWatchersCount() > r2.getWatchersCount();
                    boolean isEqual = !isGreater && r1.getWatchersCount() == r2.getWatchersCount();

                    return isEqual ? 0 : isGreater ? -1 : 1;
                }

                return r1.getForkCount() > r2.getForkCount() ? -1 : 1;
            }
        });
    }

    public class RepoViewHolder extends RecyclerView.ViewHolder {

        public GitHubRepository repository;

        private final View mMainView;

        private final TextView mRepoNameView;
        private final TextView mRepoDescriptionView;
        private final TextView mForksCountView;
        private final TextView mWatchersCountView;

        public RepoViewHolder(View itemView) {
            super(itemView);

            mMainView = itemView;

            mRepoNameView = (TextView) itemView.findViewById(R.id.tv_name);
            mRepoDescriptionView = (TextView) itemView.findViewById(R.id.tv_description);
            mForksCountView = (TextView) itemView.findViewById(R.id.tv_forks_count);
            mWatchersCountView = (TextView) itemView.findViewById(R.id.tv_watchers_count);
        }

        public void bindView(int position) {
            mRepoNameView.setText(repository.getName());
            mRepoDescriptionView.setText(repository.getDescription());

            String watchersCount = mRecyclerInstance.getContext().getString(R.string.label_watchers_count) + repository.getWatchersCount();
            String forksCount = mRecyclerInstance.getContext().getString(R.string.label_forks_count) + repository.getForkCount();
            mWatchersCountView.setText(watchersCount);
            mForksCountView.setText(forksCount);

            mMainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onItemClicked(repository);
                    }
                }
            });
        }
    }
}
