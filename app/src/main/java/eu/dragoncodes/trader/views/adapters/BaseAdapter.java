package eu.dragoncodes.trader.views.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T extends RecyclerView.ViewHolder, V> extends RecyclerView.Adapter<T> {

    protected List<V> mItems;
    protected OnItemClickedListener<V> mListener;

    BaseAdapter() {
        mItems = new ArrayList<>();
    }

    public void setItems(List<V> items) {
        mItems = items;

        postProcessItems();

        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickedListener<V> listener){
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    protected abstract void postProcessItems();

    public interface OnItemClickedListener<V> {
        void onItemClicked(V item);
    }
}
