package eu.dragoncodes.trader.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.dragoncodes.trader.R;
import eu.dragoncodes.trader.models.GitHubUser;
import eu.dragoncodes.trader.views.UrlImageView;

public class UserListAdapter extends BaseAdapter<UserListAdapter.UserViewHolder, GitHubUser> {

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void setItems(List<GitHubUser> items) {

        Set<Long> ids = new HashSet<>();

        int oldSize = mItems.size();

        for (int i = 0; i < mItems.size(); i++) {
            ids.add(mItems.get(i).getId());
        }

        for (int i = 0; i < items.size(); i++) {
            if (ids.contains(items.get(i).getId()))
                continue;

            mItems.add(items.get(i));
        }

        notifyItemRangeInserted(oldSize, mItems.size());
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.mGitHubUser = mItems.get(position);
        holder.bindView(position);
    }

    @Override
    protected void postProcessItems() {

    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        public GitHubUser mGitHubUser;

        private final View mMainView;
        private final UrlImageView mAvatar;
        private final TextView mNameView;

        public UserViewHolder(View itemView) {
            super(itemView);

            mMainView = itemView;
            mAvatar = (UrlImageView) itemView.findViewById(R.id.iv_avatar);
            mNameView = (TextView) itemView.findViewById(R.id.tv_name);
        }

        public void bindView(int position) {

            mAvatar.setUrl(mGitHubUser.getAvatarUrl());

            mNameView.setText(mGitHubUser.getUsername());

            mMainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onItemClicked(mGitHubUser);
                    }
                }
            });
        }
    }
}

