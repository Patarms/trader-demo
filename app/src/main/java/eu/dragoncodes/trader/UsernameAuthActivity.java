package eu.dragoncodes.trader;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import eu.dragoncodes.trader.utils.Constants;

public abstract class UsernameAuthActivity extends AuthenticatedActivity {

    protected String mUsername;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        if (intent.hasExtra(Constants.USERNAME_KEY)) {
            mUsername = intent.getStringExtra(Constants.USERNAME_KEY);
        }
    }
}
