package eu.dragoncodes.trader.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import eu.dragoncodes.trader.BuildConfig;
import eu.dragoncodes.trader.managers.caching.GitHubCaching;
import eu.dragoncodes.trader.models.GitHubFollower;
import eu.dragoncodes.trader.models.GitHubRepository;
import eu.dragoncodes.trader.models.GitHubUser;
import eu.dragoncodes.trader.utils.Constants;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * The middleware between the Http calls, SQL caching and the requesting party
 *
 * Most of the these calls have pagination, in normal cases I'd use an On-Demand approach letting the
 * List adapter be the one requesting specific pages, but due to lack of time this loads every page one
 * after the other until no more results are present.
 *
 * Good reads on lazy loading per user request:
 * https://guides.codepath.com/android/Endless-Scrolling-with-AdapterViews-and-RecyclerView
 *
 * Observables were the better choice instead of callbacks and listeners because the cached data
 * is now returned immediately and a request is still being made to fetch new data.
 * So in case there is a connection two onNext calls will be made: Cached -> Fetched
 *
 * TODO Diffing of fetched and cached data should be made
 * Currently the fetched data is overwriting the cached data (ON CONFLICT REPLACE). ALTER queries is a better solution I think.
 */
public class GitHubClient {

    private static final String BASE_URL = "https://api.github.com/";

    private final GitHub mService;
    private final Gson mGson;

    private final Context mContext;

    public GitHubClient(Context context) {

        mContext = context;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getClient())
                .build();

        mService = retrofit.create(GitHub.class);


        mGson = new GsonBuilder().create();
    }

    public Observable<GitHubUser> getUserDetails(@NonNull final String username) {

        return getObservable(new ObservableOnSubscribe<GitHubUser>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<GitHubUser> observableEmitter) throws Exception {

                GitHubUser cachedUser = GitHubCaching.getCachingInstance(mContext).getUser(username);

                if (cachedUser != null) {
                    observableEmitter.onNext(cachedUser);
                    observableEmitter.onComplete();

                    return;
                }

                try {
                    Response response = mService.userDetails(username).execute();

                    if (response.isSuccessful() && response.body() != null) {
                        String body = ((ResponseBody) response.body()).string();

                        GitHubUser user = mGson.fromJson(body, GitHubUser.class);

                        observableEmitter.onNext(user);
                        observableEmitter.onComplete();

                        GitHubCaching.getCachingInstance(mContext).saveUser(user);

                    } else {
                        observableEmitter.onError(new Throwable("User not found"));
                    }
                } catch (Exception e) {
                    observableEmitter.onError(e);
                }

            }
        });
    }

    public Observable<List<GitHubRepository>> getUserRepos(@NonNull final String username) {
        return getObservable(new ObservableOnSubscribe<List<GitHubRepository>>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<List<GitHubRepository>> observableEmitter) throws Exception {

                List<GitHubRepository> cachedRepos = GitHubCaching.getCachingInstance(mContext).getRepositories(username);

                if (cachedRepos.size() > 0) {
                    observableEmitter.onNext(cachedRepos);
                }

                try {
                    List<GitHubRepository> result = new ArrayList<>();
                    JsonArray reposArray = new JsonArray();

                    int page = 1;

                    do {

                        Response response = mService.userRepos(username, page).execute();

                        if (response.isSuccessful() && response.body() != null) {
                            String body = ((ResponseBody) response.body()).string();

                            JsonElement jsonContent = mGson.fromJson(body, JsonElement.class);

                            if (jsonContent.isJsonArray()) {
                                reposArray = jsonContent.getAsJsonArray();

                                if (reposArray.size() <= cachedRepos.size()) {
                                    observableEmitter.onComplete();
                                    return;
                                }

                                for (int i = 0; i < reposArray.size(); i++) {
                                    GitHubRepository repo = mGson.fromJson(reposArray.get(i), GitHubRepository.class);

                                    GitHubCaching.getCachingInstance(mContext).saveRepository(repo);

                                    result.add(repo);
                                }
                            }

                            observableEmitter.onNext(result);
                        } else {
                            observableEmitter.onError(new Throwable("Repos not found"));
                        }

                        page++;
                    } while (reposArray.size() > 0);

                    observableEmitter.onComplete();

                } catch (Exception e) {
                    e.printStackTrace();

                    if (!observableEmitter.isDisposed())
                        observableEmitter.onError(new Throwable(e.getMessage()));
                }
            }
        });
    }

    public Observable<List<GitHubUser>> getFollowers(@NonNull final String username) {
        return getObservable(new ObservableOnSubscribe<List<GitHubUser>>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<List<GitHubUser>> observableEmitter) throws Exception {

                List<GitHubFollower> cachedFollowers = GitHubCaching.getCachingInstance(mContext).getFollowers(username);

                List<GitHubUser> cachedUserResult = new ArrayList<>();
                for (int i = 0; i < cachedFollowers.size(); i++) {
                    GitHubUser cachedFollowerUser = GitHubCaching.getCachingInstance(mContext).getUser(cachedFollowers.get(i).getFollowerUsername());

                    if (cachedFollowerUser != null) {
                        cachedUserResult.add(cachedFollowerUser);
                    }
                }

                if (cachedUserResult.size() > 0) {
                    observableEmitter.onNext(cachedUserResult);
                }

                try {

                    JsonArray userArray = new JsonArray();
                    int page = 1;
                    do {
                        Response response = mService.userFollowers(username, page).execute();

                        if (response.isSuccessful() && response.body() != null) {
                            String body = ((ResponseBody) response.body()).string();
                            JsonElement jsonContent = mGson.fromJson(body, JsonElement.class);

                            List<GitHubUser> result = new ArrayList<>();

                            if (jsonContent.isJsonArray()) {
                                userArray = jsonContent.getAsJsonArray();

                                if (cachedUserResult.size() == userArray.size() && userArray.size() > 0) {
                                    observableEmitter.onComplete();
                                    return;
                                }

                                for (int i = 0; i < userArray.size(); i++) {
                                    GitHubUser user = mGson.fromJson(userArray.get(i), GitHubUser.class);

                                    GitHubCaching.getCachingInstance(mContext).saveFollower(new GitHubFollower(user.getUsername(), username));

                                    // Get the full user info when offline mode kicks in
                                    getUserDetails(user.getUsername()).subscribe();

                                    result.add(user);
                                }
                            }

                            observableEmitter.onNext(result);

                            page++;

                        } else {
                            observableEmitter.onError(new Throwable("Users not found"));

                            return;
                        }
                    } while (userArray.size() > 0);

                    observableEmitter.onComplete();
                } catch (Exception e) {
                    observableEmitter.onError(e);
                }

            }
        });
    }

    public Observable<List<GitHubUser>> getFollowing(@NonNull final String username) {
        return getObservable(new ObservableOnSubscribe<List<GitHubUser>>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<List<GitHubUser>> observableEmitter) throws Exception {

                List<GitHubFollower> cachedFollowing = GitHubCaching.getCachingInstance(mContext).getFollowing(username);

                List<GitHubUser> cachedUserResult = new ArrayList<>();
                for (int i = 0; i < cachedFollowing.size(); i++) {
                    GitHubUser cachedFollowerUser = GitHubCaching.getCachingInstance(mContext).getUser(cachedFollowing.get(i).getFollowingUsername());

                    if (cachedFollowerUser != null) {
                        cachedUserResult.add(cachedFollowerUser);
                    }
                }

                if (cachedUserResult.size() > 0) {
                    observableEmitter.onNext(cachedUserResult);
                }

                try {
                    JsonArray userArray = new JsonArray();
                    int page = 1;
                    do {

                        Response response = mService.userFollowing(username, page).execute();

                        if (response.isSuccessful() && response.body() != null) {
                            String body = ((ResponseBody) response.body()).string();
                            JsonElement jsonContent = mGson.fromJson(body, JsonElement.class);

                            final List<GitHubUser> result = new ArrayList<>();

                            if (jsonContent.isJsonArray()) {
                                userArray = jsonContent.getAsJsonArray();

                                if (cachedUserResult.size() == userArray.size()) {
                                    observableEmitter.onComplete();
                                    return;
                                }


                                for (int i = 0; i < userArray.size(); i++) {
                                    GitHubUser user = mGson.fromJson(userArray.get(i), GitHubUser.class);

                                    // Get the full user info when offline mode kicks in
                                    getUserDetails(user.getUsername()).subscribe();

                                    result.add(user);
                                }
                            }

                            observableEmitter.onNext(result);

                            GitHubCaching.getCachingInstance(mContext).saveFollowers(username, result);

                        } else {
                            observableEmitter.onError(new Throwable("Users not found"));
                        }

                        page++;
                    } while (userArray.size() > 0);

                    observableEmitter.onComplete();
                } catch (Exception e) {
                    observableEmitter.onError(e);
                }
            }
        });
    }

    private static <T> Observable<T> getObservable(ObservableOnSubscribe<T> observableOnSub) {
        return Observable.create(observableOnSub).materialize().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).dematerialize();
    }

    private OkHttpClient getClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request();

                HttpUrl url = request.url().newBuilder()
                        .addQueryParameter(Constants.GITHUB_CLIENT_ID_KEY, BuildConfig.GITHUB_CLIENT_ID)
                        .addQueryParameter(Constants.GITHUB_CLIENT_SECRET_KEY, BuildConfig.GITHUB_CLIENT_SECRECT)
                        .build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });

        return builder.build();
    }
}
