package eu.dragoncodes.trader.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface GitHub {

    @GET("users/{user}")
    Call<ResponseBody> userDetails(@Path("user") String user);

    @GET("users/{user}/repos")
    Call<ResponseBody> userRepos(@Path("user") String user, @Query("page") int page);

    @GET("users/{user}/followers")
    Call<ResponseBody> userFollowers(@Path("user") String user, @Query("page") int page);

    @GET("users/{user}/following")
    Call<ResponseBody> userFollowing(@Path("user") String user, @Query("page") int page);
}
