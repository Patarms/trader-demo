package eu.dragoncodes.trader.models;

public interface OnUserAuthenticatedListener {
    void userAuthenticated();
}
