package eu.dragoncodes.trader.models;


import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class GitHubUser extends ContentValueParcelable {

    @SerializedName("login")
    private String mUsername;

    @SerializedName("avatar_url")
    private String mAvatarUrl;

    @SerializedName("name")
    private String mName;

    @SerializedName("bio")
    private String mBio;

    @SerializedName("location")
    private String mLocation;

    @SerializedName("public_repos")
    private int mPublicReposCount;

    @SerializedName("followers")
    private int mFollowersCount;

    @SerializedName("following")
    private int mFollowingCount;

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public String getName() {
        return mName;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getBio() {
        return mBio;
    }

    public String getLocation() {
        return mLocation;
    }

    public int getPublicReposCount() {
        return mPublicReposCount;
    }

    public int getFollowersCount() {
        return mFollowersCount;
    }

    public int getFollowingCount() {
        return mFollowingCount;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put("id", getId());
        values.put("avatar_url", getAvatarUrl());
        values.put("bio", getBio());
        values.put("name", getName());
        values.put("login", getUsername());
        values.put("location", getLocation());
        values.put("public_repos", getPublicReposCount());
        values.put("following", getFollowingCount());
        values.put("followers", getFollowersCount());

        return values;
    }

    public static GitHubUser fromCursor(Cursor cursor) {
        try {

            JsonObject json = new JsonObject();

            json.addProperty("id", cursor.getLong(cursor.getColumnIndexOrThrow("id")));
            json.addProperty("avatar_url", cursor.getString(cursor.getColumnIndexOrThrow("avatar_url")));
            json.addProperty("bio", cursor.getString(cursor.getColumnIndexOrThrow("bio")));
            json.addProperty("name", cursor.getString(cursor.getColumnIndexOrThrow("name")));
            json.addProperty("login", cursor.getString(cursor.getColumnIndexOrThrow("login")));
            json.addProperty("location", cursor.getString(cursor.getColumnIndexOrThrow("location")));
            json.addProperty("public_repos", cursor.getInt(cursor.getColumnIndexOrThrow("public_repos")));
            json.addProperty("following", cursor.getInt(cursor.getColumnIndexOrThrow("following")));
            json.addProperty("followers", cursor.getInt(cursor.getColumnIndexOrThrow("followers")));

            return new GsonBuilder().create().fromJson(json, GitHubUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
