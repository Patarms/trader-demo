package eu.dragoncodes.trader.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class GitHubRepository extends ContentValueParcelable {

    @SerializedName("owner")
    private GitOwner mOwner;

    @SerializedName("html_url")
    private String mUrl;

    @SerializedName("name")
    private String mRepoName;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("watchers_count")
    private int mWatchersCount;

    @SerializedName("forks_count")
    private int mForkCount;

    public long getId() {
        return mId;
    }

    public String getName() {
        return mRepoName;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getWatchersCount() {
        return mWatchersCount;
    }

    public int getForkCount() {
        return mForkCount;
    }

    public GitOwner getOwner() {
        return mOwner;
    }

    public String getUrl() {
        return mUrl;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put("id", getId());
        values.put("html_url", getUrl());
        values.put("name", getName());
        values.put("owner_login", getOwner().getLogin());
        values.put("description", getDescription());
        values.put("watchers_count", getWatchersCount());
        values.put("forks_count", getForkCount());

        return values;
    }

    public static GitHubRepository fromCursor(Cursor cursor) {
        try {

            JsonObject json = new JsonObject();

            json.addProperty("id", cursor.getLong(cursor.getColumnIndexOrThrow("id")));
            json.addProperty("name", cursor.getString(cursor.getColumnIndexOrThrow("name")));
            json.addProperty("html_url", cursor.getString(cursor.getColumnIndexOrThrow("html_url")));
            json.addProperty("description", cursor.getString(cursor.getColumnIndexOrThrow("description")));
            json.addProperty("watchers_count", cursor.getString(cursor.getColumnIndexOrThrow("watchers_count")));
            json.addProperty("forks_count", cursor.getString(cursor.getColumnIndexOrThrow("forks_count")));

            return new GsonBuilder().create().fromJson(json, GitHubRepository.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
