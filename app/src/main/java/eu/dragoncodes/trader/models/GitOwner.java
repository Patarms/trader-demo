package eu.dragoncodes.trader.models;

import com.google.gson.annotations.SerializedName;

public class GitOwner {

    @SerializedName("login")
    private String mOwnerLogin;

    public String getLogin() {
        return mOwnerLogin;
    }
}
