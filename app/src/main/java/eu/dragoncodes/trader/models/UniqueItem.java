package eu.dragoncodes.trader.models;

import com.google.gson.annotations.SerializedName;

public abstract class UniqueItem {

    @SerializedName("id")
    protected long mId;

    public long getId() {
        return mId;
    }
}
