package eu.dragoncodes.trader.models;

import android.content.ContentValues;

public abstract class ContentValueParcelable extends UniqueItem {
    public abstract ContentValues toContentValues();
}
