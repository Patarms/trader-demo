package eu.dragoncodes.trader.models;

import android.content.ContentValues;

public class GitHubFollower extends ContentValueParcelable {

    public GitHubFollower(String followerUsername, String followingUsername) {
        setFollowerUsername(followerUsername);
        setFollowingUsername(followingUsername);
    }

    public GitHubFollower() {

    }

    private String mFollowerUsername;
    private String mFollowingUsername;

    public String getFollowerUsername() {
        return mFollowerUsername;
    }

    public void setFollowerUsername(String mFollowerUsername) {
        this.mFollowerUsername = mFollowerUsername;
    }

    public String getFollowingUsername() {
        return mFollowingUsername;
    }

    public void setFollowingUsername(String mFollowingUsername) {
        this.mFollowingUsername = mFollowingUsername;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put("follower_username", getFollowerUsername());
        values.put("following_username", getFollowingUsername());

        return values;
    }
}
