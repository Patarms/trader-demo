package eu.dragoncodes.trader;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import eu.dragoncodes.trader.managers.AccountManager;
import eu.dragoncodes.trader.models.GitHubUser;
import eu.dragoncodes.trader.network.GitHubClient;
import io.reactivex.functions.Consumer;

/**
 * Given the LoginScreen description of "A simple screen with input field for username and a login button."
 * My understanding was that there was no actual authentication and thus no need for token creation or basic auth
 * processes.
 *
 * This class queries the github api for a user with the provided username, if such is found, that is considered to be
 * the logged in user. Preservation of login state is done via a saved "username" in shared preferences @link({@link AccountManager}), so no
 * session duration can be found.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText mUsernameView;
    private View mProgressView;
    private View mLoginFormView;

    private TextView mErrorMessageView;

    private final Consumer<GitHubUser> mOnNextLoginAction = new Consumer<GitHubUser>() {
        @Override
        public void accept(@io.reactivex.annotations.NonNull GitHubUser githubUser) throws Exception {

            showProgress(false);

            finishAuthentication(githubUser);
        }
    };

    private final Consumer<Throwable> mOnErrorLoginAction = new Consumer<Throwable>() {
        @Override
        public void accept(Throwable error) throws Exception {
            showProgress(false);

            showError(error);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button mEmailSignInButton = (Button) findViewById(R.id.btn_login);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mUsernameView = (EditText) findViewById(R.id.et_username);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mErrorMessageView = (TextView) findViewById(R.id.tv_error);
    }

    private void gitHubLogin(@NonNull final String username) {
        showError(null);

        new GitHubClient(this).getUserDetails(username)
                .subscribe(mOnNextLoginAction, mOnErrorLoginAction);
    }

    private void attemptLogin() {

        mUsernameView.setError(null);

        String username = mUsernameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }


        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            gitHubLogin(username);
        }
    }

    private void showError(Throwable error) {
        if (error != null) {
            mErrorMessageView.setVisibility(View.VISIBLE);

            mErrorMessageView.setText(getString(R.string.error_no_user_found));

        } else {
            mErrorMessageView.setVisibility(View.GONE);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void finishAuthentication(GitHubUser user) {
        AccountManager manager = new AccountManager(this);
        manager.setUserId(user.getId());
        manager.setUsername(user.getUsername());

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}

